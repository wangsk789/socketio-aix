### socket.io chat

backend for chatting, based on socket.io

main function: 

connect(nsp, username);
disconnect();

startMatch();
cancelMatch();

joinRoom(roomname);
leaveRoom(roomname);

sendRoomMessage(roomname, content);
sendPrivateMessage(receiver, message);
sendPublicMessage(message);

setMaxUser(maxuser);
